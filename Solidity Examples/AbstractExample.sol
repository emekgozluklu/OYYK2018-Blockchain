pragma solidity 0.4.24;

interface Driveable{
    function Drive() returns(bool);
}

contract Vehicle is Driveable{
    
    function isAbstract() returns(bool);
    
}

contract TwoWheeledVehicle is Vehicle{
    
}

contract FourWheeledVehicle is Vehicle{
    
}

contract Bicycle is TwoWheeledVehicle{
    
}

contract Car is FourWheeledVehicle{
    
}

contract Motorcycle is TwoWheeledVehicle{
    
}
