pragma solidity 0.4.24;

contract TimeStamp{
    
    uint lastUpdated = now;
    
    function update() returns(uint){
        if(now - lastUpdated >= 5 * 60){
            lastUpdated = now;
            
        }   
    
    return lastUpdated;
    }
}

