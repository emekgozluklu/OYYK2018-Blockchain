pragma solidity ^0.4.24;

contract Sanitarian{
    
}

contract Doctor is Sanitarian{
    
}

contract Surgeon is Doctor{
    
}

contract Practitioner is Doctor{
    
}

contract Nurse is Sanitarian{
    
}

contract Dentist is Doctor{
    
}
