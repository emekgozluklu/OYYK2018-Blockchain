pragma solidity 0.4.24;
pragma experimental ABIEncoderV2;

contract Structs{
    
    struct Person{
        uint age;
        string name;
        bool isAdult;
        bytes32 password;
        
    }
    
    mapping (uint => Person) PeopleMap;
    
    
    function generatePerson(uint _id, string _name, uint _age, string _password) returns(Person){
        
        Person p;
        p.name = _name;
        p.age = _age;
        if(p.age > 18) p.isAdult = true;
        p.password = keccak256(_password);
        PeopleMap[_id] = p;
        return p;
    }
    
    function isValid() returns(bool){
        return true;
    }
}
