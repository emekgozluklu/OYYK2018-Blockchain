pragma solidity 0.4.24;

contract Emek{
    event Print(uint _body);
    
    
    function reverse(uint[] _array) returns(uint[]){
        uint[] reverseArray;

        for(uint i=_array.length;i>0; i--){
            reverseArray.push(_array[i-1]);
            }
    
        return reverseArray;
    }
}
