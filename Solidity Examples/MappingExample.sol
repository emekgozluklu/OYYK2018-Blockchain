pragma solidity 0.4.24;
pragma experimental ABIEncoderV2;

contract MappingExample{
    
    mapping (uint => string) exampleMap;
    
    function generateMapFromArrays(uint[] _keys, string[] _values){
        
        for(uint i=0; i<_keys.length; i++){
            exampleMap[_keys[i]] = _values[i];
        }
    }
}
