pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

interface Drinkable{
    function drink() returns(bool);
}
    
interface Eatable{
    function eat() returns(bool);
}

contract Food {
    
    
    struct Nutrition {
        string name;
        uint value;
    }
    
    enum Taste {
        TATLI, NOTR, TUZLU, ACI
    }
    
    string internal name;
    uint internal calorie;
    Nutrition[] internal nutritions;
    uint internal weigth;
    Taste internal taste;
    string internal origin;
    bool internal isCooked;
    bool internal isProcessed;
    
    function getName() public returns (string) {
        return name;
    }
    
    function setName(string _name) public {
        name = _name;
    }
    
    function setCalorie(uint _calorie){
        calorie = _calorie;
    }
    
    
    function setWeight(uint _weight){
        weigth = _weight;
    }
    
    function setTaste(Taste _taste){
        taste = _taste;
    }
    
    function setOrigin(string _origin){
        origin = _origin;
    }
    
    function set_isCooded(bool _isCooked){
        isCooked = _isCooked;
    }
    
}

contract NonAlcoholDrink is Food, Drinkable{
    
    enum DrinkColor {
        BLACK, YELLOW, UNCOLORED
    }
    
    DrinkColor drinkColor;
    bool isSoda = isCooked;
    bool isSugared;
    
}

contract Coke is NonAlcoholDrink {
    
    enum CokeBrand {
        COCA_COLA, PEPSI, LE_COLA, COLA_TURKA
    }
    
    bool isLight;
}

contract AlcoholDrink is Food, Drinkable {
    
    uint alcoholRatio;
    
    
}

contract Vegetable is Food, Eatable{
    
    enum VegetableSeason {
        WINTER_VEGETABLE, SUMMER_VEGETABLE
    }
    
    VegetableSeason vegetableSeason;
    
}

contract Meat is Food, Eatable{
    
    enum CookingType {
        LESS, MORE
    }
    
    bool isRedMeat;
    CookingType cookingType;
    
    
}

contract Dessert is Food, Eatable{
    
    bool isSerbetli;
    
}

contract Bean is Vegetable {
    bool isKuru;
}

contract Spinach is Vegetable {
    
}

contract Burger is Meat {
    bool isVegan;
}

contract Katmer is Dessert {
    bool isLegal;
}

contract Ayran is NonAlcoholDrink {
    bool isAcik;
}

contract Beer is AlcoholDrink {
    
    enum BeerBrand {
        EFES, TUBORG, CARLSBERG, GUINNESS,
        MARMARA34, SKOL, WEIHNENASDFLKJG, BECKS, AMSTERDAM, MILLER, ZIKKIM, GARAGUZU
    }
    
    bool isTombulEfes;
    BeerBrand beerBrand;
}

contract Wine is AlcoholDrink {
    enum WineColor {
        RED, WHITE, ROSE
    }
    
    bool isKopekOlduren;
    WineColor wineColor;
    
}

contract Falafel is Bean {
    
}

contract RoastedSpinach is Spinach {
    
}

