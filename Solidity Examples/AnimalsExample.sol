pragma solidity 0.4.24;

interface Carnivore{
    function eatsMeat() external returns(bool);
}

interface Herbivore{
    function DoesntEatMeat() external returns(bool);
}

contract Animal{
    function is_Abstract() private returns(bool);
    bool flies;
    bool swims;
    
}

contract Mammal is Animal{
    function is_Abstract() private returns(bool);
    uint legs;
}

contract Bird is Animal,Herbivore, Carnivore{
    uint wing_width;

    function setBools(bool _flies, bool _swims){
        flies = _flies;
        swims = _swims;
    }
    
}

contract Dog is Mammal, Carnivore{
    
    function setBools(bool _flies, bool _swims){
        flies = _flies;
        swims = _swims;
        }
        
    function setLegs(uint _legs){
        legs = _legs;
    }
}

contract Bear is Mammal, Carnivore{
    function setBools(bool _flies, bool _swims){
        flies = _flies;
        swims = _swims;
    }
    
    function setLegs(uint _legs){
        legs = _legs;
    }
    
}

contract Cat is Mammal, Carnivore{
    function setBools(bool _flies, bool _swims){
        flies = _flies;
        swims = _swims;
    }
    function setLegs(uint _legs){
        legs = _legs;
        
    }
}
