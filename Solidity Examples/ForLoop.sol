pragma solidity 0.4.24;

contract Loops{
    
    uint[] arr;
    
    
    function sum(uint[] _array) returns(uint){
        arr.push(5);
        arr.push(17);
        arr.push(7);
        arr.push(3);
        arr.push(9);
        
        uint sumy=0;
        
        for(uint i=0;i<_array.length;i++){
            sumy = sumy + _array[i];
        }
        
        return sumy;
    }
}
