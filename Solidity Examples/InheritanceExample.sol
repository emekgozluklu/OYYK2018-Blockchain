pragma solidity 0.4.24;

interface Executable{
    function isExecutable() external returns(bool);
}

interface Readable{
    function isReadable() external returns(bool);
}

contract File{
    
    string name;
    uint size;
    string dateCreated;
    
    function setName(string _name) internal {
        name = _name;
    }
    
    function setSize(uint _size) internal{
        size = _size;
    }
    
    function setDate(string _date) internal{
        dateCreated = _date;
    }
}

contract TextFile is File{
    string encoding;
    uint NumOfChars;
}

contract PlainTextFile is TextFile, Readable{
    string[] lines;
}

contract HTMLFile is TextFile, Executable{
    enum browser {
        Firefox, Opera, Chrome, Edge
    }
    
}


contract BinaryFile is File, Executable{
}

contract DataFile is BinaryFile{
    
}
