pragma solidity 0.4.24;

contract Fibonacci{

    function fibonacci(uint _term) returns(uint){
        if (_term == 0){
            return 0;
        }
        else if (_term == 1){
            return 1;
        }
        else{
            return fibonacci(_term - 1) + fibonacci(_term - 2);
        }
    }

}
