pragma solidity 0.4.24;
pragma experimental ABIEncoderV2;

contract Mapping{
    
    enum Color {
        BLACK, WHITE, YELLOW, TEQIR
    }
    
    enum Personality {
        PSYCHO, CUTE
    }
    
    struct Cat{
        string name;
        uint age;
        Color color;
        Personality personality;
    }
    
    mapping (uint => Cat) myMapExample;
    
    function TryMapping()returns(Cat){
        
        Cat c;
        c.name = 'bebe';
        c.age = 5;
        c.color = Color.TEQIR;
        c.personality = Personality.PSYCHO;
        
        myMapExample[1] = c; 
        return myMapExample[1];
        
    }
    
}
