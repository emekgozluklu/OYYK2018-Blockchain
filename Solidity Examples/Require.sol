pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

contract People {
    
  
  struct Person {
      string name;
      uint age;
      bool isAdult;
      bytes32 password;
  }
  
  event PrintPerson(Person p);
  
  mapping(uint => Person) people;
  
  function addPerson(string[] _names, uint[] _ages) {
      
        for(uint i = 0; i < _names.length; i++){
            Person p;
            p.name = _names[i];
            p.age = _ages[i];
          if(p.age >= 18) {
              p.isAdult = true;
          }
          
          people[i] = p;
        }

          
          
          PrintPerson(p);

  }
  
  function isValid(uint _index, string _password) returns (bool) {
     
     Person selectedPerson = people[_index];
     
     if (selectedPerson.password == keccak256(_password)) {
         return true;
     }
     
     else return false;
      
  }
  
  function requireTest(uint _number) returns (uint) {
      require(_number > 5);
      return _number;
  }
  modifier arraysShouldBeEqual(uint _number){
      require(
          _number>5,
          "Your input must be greater than 5"
        );
          _;
  }
  
}






