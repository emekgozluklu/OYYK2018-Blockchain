pragma solidity 0.4.24;

contract ExampleContract{
    
    uint age;
    string name;

    constructor(string _name, uint _age){
        name = _name;
        age = _age;
    }
    
}
// Bir contract icin 2 tane constructor tanımlayamayız.
contract AnotherContract{
    ExampleContract i = new ExampleContract("emek", 21);
}
